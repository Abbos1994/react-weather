import React, {useEffect, useState} from 'react'
import {
    Col,
    Collapse,
    Container, DropdownItem, DropdownMenu, DropdownToggle,
    Nav,
    Navbar,
    NavbarText,
    NavbarToggler,
    NavItem,
    NavLink,
    Row,
    UncontrolledDropdown,
    Button
} from "reactstrap"
import {Line} from "react-chartjs-2"
import DatePicker from "react-datepicker"
import axios from 'axios';

// import "react-datepicker/dist/react-datepicker.css"
// import 'bootswatch/dist/simplex/bootstrap.min.css'
import './Styles.scss'

function App(props) {
    const [startDate, setStartDate] = useState(new Date(Date.now()));
    const [endDate, setEndDate] = useState(new Date(Date.now()));
    const [PM1_dataPoints, set_PM1_dataPoints] =useState([]);
    const [PM25_dataPoints, set_PM25_dataPoints] =useState([]);
    const [PM10_dataPoints, set_PM10_dataPoints] =useState([]);
    const [CO2_dataPoints, set_CO2_dataPoints] =useState([]);
    const [Temperature_dataPoints, set_Temperature_dataPoints] =useState([]);
    const [Humidity_dataPoints, set_Humidity_dataPoints] =useState([]);
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);

    let a_var = startDate.toISOString();
    let b_var = endDate.toISOString();

    //get PM 1, 2.5 and 10
    useEffect(() => {
        axios.get(`http://104.248.198.113:8000/getPMs/?time_start=${a_var}&time_end=${b_var}`)
            .then(res => {
                if(res.data.length < 1) {
                    set_PM1_dataPoints([])
                    set_PM25_dataPoints([])
                    set_PM10_dataPoints([])
                }
                for (let i = 0; i < res.data.length; i++) {
                    let gom = new Date(res.data[i].time)
                    set_PM1_dataPoints(old => [...old, {x: gom.toISOString(), y: res.data[i].pm1}])
                    set_PM25_dataPoints(old => [...old, {x: gom.toISOString(), y: res.data[i].pm25}])
                    set_PM10_dataPoints(old => [...old, {x: gom.toISOString(), y: res.data[i].pm10}])
                }
            })
    }, [a_var, b_var])

    //get CO2
    useEffect(() => {
        axios.get(`http://104.248.198.113:8000/getCO2/?time_start=${a_var}&time_end=${b_var}`)
            .then(res => {
                if(res.data.length < 1) {
                    set_CO2_dataPoints([])
                }
                for (let i = 0; i < res.data.length; i++) {
                    let gom = new Date(res.data[i].time)
                    set_CO2_dataPoints(old => [...old, {x: gom.toISOString(), y: res.data[i].index}])
                }
            })
    }, [a_var, b_var])

    //get Temperature
    useEffect(() => {
        axios.get(`http://104.248.198.113:8000/getTemperature/?time_start=${a_var}&time_end=${b_var}`)
            .then(res => {
                if(res.data.length < 1) {
                    set_Temperature_dataPoints([])
                }
                for (let i = 0; i < res.data.length; i++) {
                    let gom = new Date(res.data[i].time)
                    set_Temperature_dataPoints(old => [...old, {x: gom.toISOString(), y: res.data[i].degree}])
                }
            })
    }, [a_var, b_var])

    // get Humudity
    useEffect(() => {
        axios.get(`http://104.248.198.113:8000/getHumidity/?time_start=${a_var}&time_end=${b_var}`)
            .then(res => {
                if(res.data.length < 1) {
                    set_Humidity_dataPoints([])
                }
                for (let i = 0; i < res.data.length; i++) {
                    let gom = new Date(res.data[i].time)
                    set_Humidity_dataPoints(old => [...old, {x: gom.toISOString(), y: res.data[i].percent}])
                }
                console.log('humidity:',res.data);
            })
    }, [a_var, b_var])

    const PMData = {
        datasets: [
            {
                label: 'PM 1.0',
                data: PM1_dataPoints,
                fill: false,
                backgroundColor: 'grey',
                borderColor: 'grey',
            },
            {
                label: 'PM 2.5',
                data: PM25_dataPoints,
                fill: false,
                backgroundColor: 'grey',
                borderColor: 'rgba(66, 135, 245)',
            },
            {
                label: 'PM 10',
                data: PM10_dataPoints,
                fill: false,
                backgroundColor: 'grey',
                borderColor: 'rgba(26, 135, 134)',
            },
        ],
    }
    const PMOptions = {
        scales: {
            yAxes: [{
                ticks:
                    {
                        beginAtZero: true
                    }
            }],
            xAxes: [{
                type: 'time',
                time: {
                    displayFormats: {
                        minute: 'YYYY MMM D h:mm:ss a'
                    },
                    unit: 'minute'
                }
            }]
        }
    }
    const CO2Data = {
        datasets: [
            {
                label: 'CO2',
                data: CO2_dataPoints,
                fill: true,
                backgroundColor: 'rgb(99,250,255)',
                borderColor: 'rgba(248,0,0,0.2)',
            },
        ],
    }
    const CO2Options = {
        scales: {
            yAxes: [{
                ticks:
                    {
                        beginAtZero: true
                    }
            }],
            xAxes: [{
                type: 'time',
                time: {
                    displayFormats: {
                        minute: 'YYYY MMM D h:mm:ss a'
                    },
                    unit: 'minute'
                }
            }]
        }
    }
    const TemperatureData = {
        datasets: [
            {
                label: 'Temperature',
                data: Temperature_dataPoints,
                fill: false,
                backgroundColor: 'red',
                borderColor: 'red',
            },
        ],
    }
    const TemperatureOptions = {
        scales: {
            yAxes: [{
                ticks:
                    {
                        beginAtZero: true
                    }
            }],
            xAxes: [{
                type: 'time',
                time: {
                    displayFormats: {
                        minute: 'YYYY MMM D h:mm:ss a'
                    },
                    unit: 'minute'
                }
            }]
        }
    }
    const HumidityData = {
        datasets: [
            {
                label: 'Humidity',
                data: Humidity_dataPoints,
                fill: false,
                backgroundColor: 'blue',
                borderColor: 'blue',
            },
        ],
    }
    const HumidityOptions = {
        scales: {
            yAxes: [{
                ticks:
                    {
                        beginAtZero: true
                    }
            }],
            xAxes: [{
                type: 'time',
                time: {
                    displayFormats: {
                        minute: 'YYYY MMM D h:mm:ss a'
                    },
                    unit: 'minute'
                }
            }]
        }
    }
    function toggleBtn(){
        alert('on AIR');
    }
    return (
        <Container fluid>
            <Navbar className={'border-0'} expand="lg">
                <NavbarToggler className={'bg-danger text-success'} onClick={toggle}/>
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="mr-auto " navbar>
                        <NavItem>
                            <NavLink href="#">Components</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="https://gitlab.com/Abbos1994/react-weather">GitLab</NavLink>
                        </NavItem>
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav caret>
                                Options
                            </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem>
                                    Option 1
                                </DropdownItem>
                                <DropdownItem>
                                    Option 2
                                </DropdownItem>
                                <DropdownItem divider/>
                                <DropdownItem>
                                    Reset
                                </DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                        <NavItem>
                            <DatePicker className={'form-control'} selected={startDate}
                                        onChange={date => setStartDate(date)}/>
                            <DatePicker className={'form-control m-2'} selected={endDate}
                                        onChange={date => setEndDate(date)}/>
                        </NavItem>
                        <Button className="m-2" color="danger" onClick={toggleBtn}>On-Line</Button>
                    </Nav>
                    <NavbarText>Simple Text</NavbarText>
                </Collapse>
            </Navbar>
            <Row>
                <Col md={12}>
                    <Row>
                        <Col md={6}>
                            <Line data={PMData} options={PMOptions}/>
                        </Col>
                        <Col md={6}>
                            <Line data={CO2Data} options={CO2Options}/>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <Line data={TemperatureData} options={TemperatureOptions}/>
                        </Col>
                        <Col md={6}>
                            <Line data={HumidityData} options={HumidityOptions}/>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Container>
    );
}

export default App;